import re


class VectorLengthError(Exception):
    """Raised when the input vector length is not match or empty"""
    pass


def validate_key_names(cluster: dict):
    for key in cluster.keys():
        if not re.match('group_\d', key):
            raise KeyError('Key name does not match the expected pattern')
    return True


def process_and_validate_vectors(vectors_list: list, vector_length: int = None):
    for row in vectors_list:
        if type(row) is list:
            for element in row:
                if type(element) == list:
                    if vector_length == None:
                        vector_length = len(element)
                    elif vector_length == len(element):
                        process_and_validate_vectors(element, vector_length)
                    else:
                        raise VectorLengthError(
                            'Vector length does not math, or empty')
    return True

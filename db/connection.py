import pymongo

client = pymongo.MongoClient('mongodb://admin:admin@localhost:27017/')

db = client['cluster']

original = db['original']
filtered = db['filtered']

def get_all_document(collection: str) -> list:
    document_list = []
    for document in db[collection].find({}):
        document_list.append(document)
    return document_list

def insert_cluster_to_db(collection: str, cluster_data: dict):
        db[collection].insert_one(cluster_data)
    
from flask import Flask
import os

from api.routes import api


def create_app():
    app = Flask(__name__)
    app.register_blueprint(api)

    return app


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=int(os.environ.get("PORT", 5000)))


from subprocess import Popen, PIPE
from sys import platform

runnable_per_platforms = {
    'linux': 'lexunit-exercise-linux-amd64',
    'linux2': 'lexunit-exercise-linux-amd64',
    'darwin': 'lexunit-exercise-darwin-amd64',
    'win32': 'lexunit-exercise-windows-amd64'
}


def get_runnable_per_platform_type():
    if platform in runnable_per_platforms:
        return runnable_per_platforms[platform]
    else:
        raise KeyError('Error in platform')


def process_cluster_data(cluster: dict, verbose_level='1.0') -> dict:
    file_path = f'./cluster_data_algorithm/{get_runnable_per_platform_type()}'

    result = Popen([file_path, cluster, verbose_level],
                   stdin=PIPE, stdout=PIPE, stderr=PIPE)
    return result.communicate()

from flask import Blueprint, request, jsonify, Response
from cluster import VectorLengthError, validate_key_names, process_and_validate_vectors
from data_process import process_cluster_data
from db.connection import get_all_document, insert_cluster_to_db

api = Blueprint('api', __name__, url_prefix='/api')


@api.route('/cluster', methods=['POST'])
def post_cluster() -> Response:
    input_data = dict(request.json)
    filtered_data = ""
    try:
        if(validate_key_names(input_data) and process_and_validate_vectors(input_data.values())):
            # filtered_data = process_cluster_data(input_data)
            insert_cluster_to_db('original', input_data)
            # insert_cluster_to_db('filtered', {filtered_data})
            return Response(input_data, status=200, mimetype='application/json')
    except VectorLengthError as vle:
        return Response(str(vle), status=400, mimetype='application/json')
    except KeyError as ke:
        return Response(str(ke), status=400, mimetype='application/json')
    except ValueError as ve:
        return Response(str(ve), status=400, mimetype='application/json')


@api.route('/cluster/<collection_name>', methods=['GET'])
def get_clusters(collection_name) -> Response:
    collection = get_all_document(str(collection_name))
    return Response(str(collection), status=200,  mimetype='application/json')


@api.route('/', methods=['GET'])
def hello():
    return {"Hello":"LexUnit :)"}
